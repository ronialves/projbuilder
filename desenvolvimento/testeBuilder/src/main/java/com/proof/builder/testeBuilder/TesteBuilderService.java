package com.proof.builder.testeBuilder;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TesteBuilderService {

    private final TesteBuilderRepository testeBuilderRepository;

    public TesteBuilderService(TesteBuilderRepository testeBuilderRepository) {this.testeBuilderRepository = testeBuilderRepository;}

    public TesteBuilderModel saveInfo(TesteBuilderModel testeBuilderModel) {
        return testeBuilderRepository.insert(testeBuilderModel);
    }

    public TesteBuilderModel updateInfo(TesteBuilderModel testeBuilderModel) {
        return testeBuilderRepository.save(testeBuilderModel);
    }

    public void deleteInfo(TesteBuilderModel testeBuilderModel) {
        testeBuilderRepository.delete(testeBuilderModel);
    }

    
    public List<TesteBuilderModel> findAll() {
        return testeBuilderRepository.findAll();
    }
    
    public List<TesteBuilderModel> findCustom(String cpf,String nome) {
    	    	
        return testeBuilderRepository.findByCpfAndNome(cpf,nome);
    }

}
