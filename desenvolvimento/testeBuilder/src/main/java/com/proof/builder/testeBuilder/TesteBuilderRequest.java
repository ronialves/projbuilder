package com.proof.builder.testeBuilder;

import java.sql.Date;

import lombok.Data;

@Data
public class TesteBuilderRequest {

    private String nome;
    private String cpf;
    private Date dataNascimento;
    
    
    public TesteBuilderModel toTesteBuilderModel(){
       return TesteBuilderModel.builder().nome(this.nome).cpf(this.cpf).dataNascimento(this.dataNascimento).build();
    }
}
