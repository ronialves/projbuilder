package com.proof.builder.testeBuilder;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;

@Component
public interface TesteBuilderRepository extends MongoRepository<TesteBuilderModel ,String> {
	
	   @Query(value = "{ 'cpf' : ?0, 'nome' : ?1 }")
	    List<TesteBuilderModel> findByCpfAndNome(String cpf, String nome);

}
