package com.proof.builder.testeBuilder;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/testeBuilder")
@Api(value="Teste Builder",description="Documentação swagger para Controller.")
public class TesteBuilderController {

    private final TesteBuilderService testeBuilderService;

    public TesteBuilderController(TesteBuilderService testeBuilderService) {this.testeBuilderService = testeBuilderService;}

    @ApiOperation(value="Cadastrar Informações")
    @PostMapping("/gravar")
    public ResponseEntity saveInfo(@RequestBody TesteBuilderRequest testeBuilderRequest) {
        log.info("Request : {}", testeBuilderRequest);
        testeBuilderService.saveInfo(testeBuilderRequest.toTesteBuilderModel());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value="Atualiza Informações")
    @GetMapping("/atualizar")
    public ResponseEntity updateInfo(@RequestBody TesteBuilderRequest testeBuilderRequest) {
        log.info("Request : {}", testeBuilderRequest);
        testeBuilderService.updateInfo(testeBuilderRequest.toTesteBuilderModel());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value="Realiza delete de Informações")
    @GetMapping("/deletar")
    public ResponseEntity deleteInfo(@RequestBody TesteBuilderRequest testeBuilderRequest) {
        log.info("Request : {}", testeBuilderRequest);
        testeBuilderService.deleteInfo(testeBuilderRequest.toTesteBuilderModel());
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }


    @ApiOperation(value="Retorna consulta por CPF e Nome")
    @GetMapping("/retornarPorCpfNome")
    public List<TesteBuilderModel> getCpfNome(@RequestParam("cpf") String cpf,@RequestParam("nome") String nome) {
        return testeBuilderService.findCustom(cpf,nome);
    }
    
    @ApiOperation(value="Retorna todas as informações da base")
    @GetMapping("/retornarTodos")
    public List<TesteBuilderModel> getAllt() {
        return testeBuilderService.findAll();
    }
}
