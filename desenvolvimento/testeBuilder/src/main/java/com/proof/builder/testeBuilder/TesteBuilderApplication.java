package com.proof.builder.testeBuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteBuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteBuilderApplication.class, args);
	}

}
