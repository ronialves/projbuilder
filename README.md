# Teste Builder 

Tecnologias Utilizadas: 

Java 1.8 
Docker version 17.05.0-ce, build 89658be

S.O Ubuntu 18

Spring Boot 2.3.4.RELEASE
Maven 

# Comando para executar API (Ambos na raiz do projeto testeBuilder)


mvn clean install package

docker-compose up

Obs.: Serão gerados e iniciada duas imagens docker (uma com a API e outra com a base MongoDB)

# URL para acesso 

Swagger
http://localhost:8080/swagger-ui.html


# Importante , na url do Swagger existem todos os payloads para serem utilizados nos teste com o Postman. 


